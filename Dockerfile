FROM alpine:edge

RUN echo "http://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories \
    && apk update \
    && apk add --no-cache alpine-sdk ca-certificates zlib-dev ghc
RUN curl -sSfL https://github.com/commercialhaskell/stack/releases/download/v1.3.2/stack-1.3.2-linux-x86_64-static.tar.gz | tar -C /usr/local/bin --strip-components=1 -xzvf - stack-1.3.2-linux-x86_64-static/stack \
    && chmod 755 /usr/local/bin/stack
